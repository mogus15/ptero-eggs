#!/bin/bash
version_egg="2.1"
version_script="2.5"
echo "⚙️  Script Version: ${version_script}"
if [[ -f "./libraries/version" ]]; then
    versions=" $(cat ./libraries/version) " 
    comm1=$( printf '%s\n' "$versions" | tr -d '.' )
    comm2=$( printf '%s\n' "${version_egg}" | tr -d '.' )
    version_system=$(cat ./libraries/version_system) 
    if [[ -f "./libraries/version_system" ]]; then
        if [ "${version_system}" = "true" ]; then
            if [ "${comm1}" -ge "${comm2}"  ]; then
                echo "✅  Updated Egg."
            else
                echo "
    
⚠️  Outdated Egg.
🔴  Installed Version: ${versions}
🟢  Latest Version: ${version_egg}
    
"
            fi
        fi
    fi
else
    echo "
    
⚠️  Outdated Egg.
🔴  Installed Version: 1.0
🟠  If you just updated the Egg, just Reinstall your Server (nothing will be deleted).
🟢  Latest Version: ${version_egg}
    
"
fi